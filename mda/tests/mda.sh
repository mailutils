# This file is part of GNU Mailutils.
# Copyright (C) 2007-2025 Free Software Foundation, Inc.
#
# GNU Mailutils is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or (at
# your option) any later version.
#
# GNU Mailutils is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>.

INPUT_MSG=$abs_top_srcdir/mda/tests/input.msg
dumpmail() {
	    sed -e '/^From /d'\
		-e /^X-IMAPbase:/d\
                -e /^X-UID:/d $1
}    
