# GNU Mailutils -- a suite of utilities for electronic mail -*- autotest -*-
# Copyright (C) 2020-2025 Free Software Foundation, Inc.
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([append])

AT_DATA([orig.mbox],
[From hare@wonder.land Mon Jul 29 22:00:08 2002
Received: (from hare@wonder.land) 
	by wonder.land id 3301
	for alice@wonder.land; Mon, 29 Jul 2002 22:00:06 +0100
Date: Mon, 29 Jul 2002 22:00:01 +0100
From: March Hare  <hare@wonder.land>
Message-Id: <200207292200.3301@wonder.land>
To: Alice  <alice@wonder.land>
Subject: Invitation
X-IMAPbase:                   10                    9
X-UID: 1

Have some wine

From alice@wonder.land Mon Jul 29 22:00:09 2002
Received: (from alice@wonder.land) 
	by wonder.land id 3302
	for hare@wonder.land; Mon, 29 Jul 2002 22:00:07 +0100
Date: Mon, 29 Jul 2002 22:00:02 +0100
From: Alice  <alice@wonder.land>
Message-Id: <200207292200.3302@wonder.land>
To: March Hare  <hare@wonder.land>
Subject: Re: Invitation
X-UID: 2

I don't see any wine

From hare@wonder.land Mon Jul 29 22:00:10 2002
Received: (from hare@wonder.land) 
	by wonder.land id 3303
	for alice@wonder.land; Mon, 29 Jul 2002 22:00:08 +0100
Date: Mon, 29 Jul 2002 22:00:03 +0100
From: March Hare  <hare@wonder.land>
Message-Id: <200207292200.3303@wonder.land>
To: Alice  <alice@wonder.land>
Subject: Re: Invitation
X-UID: 3

There isn't any

])

AT_DATA([msg],
[Received: (from alice@wonder.land) 
	by wonder.land id 3304
	for hare@wonder.land; Mon, 29 Jul 2002 22:00:09 +0100
Date: Mon, 29 Jul 2002 22:00:04 +0100
From: Alice  <alice@wonder.land>
Message-Id: <200207292200.3304@wonder.land>
To: March Hare  <hare@wonder.land>
Subject: Re: Invitation
Status: O

Then it wasn't very civil of you to offer it
])

AT_CHECK([cp orig.mbox inbox])

AT_DATA([commands],
[append msg
count
4
uid
env_date
env_sender
attr
headers
body_text
3
body_lines
body_size
body_text
])
AT_CHECK([mbop -m inbox < commands],
[0],
[append: OK
count: 4
4 current message
4 uid: 9
4 env_date: Mon Jul 29 21:00:09 2002
4 env_sender: alice@wonder.land
4 attr: -
4 headers: Received:(from alice@wonder.land) by wonder.land id 3304 for hare@wonder.land; Mon, 29 Jul 2002 22:00:09 +0100
Date:Mon, 29 Jul 2002 22:00:04 +0100
From:Alice  <alice@wonder.land>
Message-Id:<200207292200.3304@wonder.land>
To:March Hare  <hare@wonder.land>
Subject:Re: Invitation
X-UID:9

4 body_text: Then it wasn't very civil of you to offer it

3 current message
3 body_lines: 1
3 body_size: 16
3 body_text: There isn't any

])

AT_CHECK([mbop -m inbox 3\; body_lines\; body_size\; body_text],
[0],
[3 current message
3 body_lines: 1
3 body_size: 16
3 body_text: There isn't any

])

AT_CHECK([
mbop -m inbox uidvalidity
],
[0],
[uidvalidity: 10
])

AT_DATA([commands.atr],
[append -attr FRP msg
count
5
uid
env_date
env_sender
headers
body_text
])

AT_DATA([commands.ae],
[append -sender gray@gnu.org -date 'Sat Dec  4 08:00:00 2021' -attr FRP msg
count
5
uid
env_date
env_sender
attr
headers
body_text
])

AT_CHECK([mbop -m inbox < commands.ae],
[0],
[append: OK
count: 5
5 current message
5 uid: 10
5 env_date: Sat Dec  4 08:00:00 2021
5 env_sender: gray@gnu.org
5 attr: FPR
5 headers: Received:(from alice@wonder.land) by wonder.land id 3304 for hare@wonder.land; Mon, 29 Jul 2002 22:00:09 +0100
Date:Mon, 29 Jul 2002 22:00:04 +0100
From:Alice  <alice@wonder.land>
Message-Id:<200207292200.3304@wonder.land>
To:March Hare  <hare@wonder.land>
Subject:Re: Invitation
Status:FPR
X-UID:10

5 body_text: Then it wasn't very civil of you to offer it

])

#
# Test append to a mailbox whose last message is missing empty line
# terminator.  To do so, strip the last byte from the orig.mbox and
# append the message to the resulting mailbox (nl1.mbox).  The partial
# message should become properly terminated after the append.
#

AT_CHECK([bs=$(wc -c orig.mbox | sed -e 's/^ *//' -e 's/ .*//')
dd if=orig.mbox of=nl1.mbox bs=$((bs - 1)) count=1
],
[0],
[ignore],
[ignore])

AT_CHECK([mbop -m nl1.mbox < commands],
[0],
[append: OK
count: 4
4 current message
4 uid: 9
4 env_date: Mon Jul 29 21:00:09 2002
4 env_sender: alice@wonder.land
4 attr: -
4 headers: Received:(from alice@wonder.land) by wonder.land id 3304 for hare@wonder.land; Mon, 29 Jul 2002 22:00:09 +0100
Date:Mon, 29 Jul 2002 22:00:04 +0100
From:Alice  <alice@wonder.land>
Message-Id:<200207292200.3304@wonder.land>
To:March Hare  <hare@wonder.land>
Subject:Re: Invitation
X-UID:9

4 body_text: Then it wasn't very civil of you to offer it

3 current message
3 body_lines: 1
3 body_size: 16
3 body_text: There isn't any

])

AT_CHECK([mbop -m nl1.mbox 3\; body_lines\; body_size\; body_text],
[0],
[3 current message
3 body_lines: 1
3 body_size: 16
3 body_text: There isn't any

])

#
# Test append to a mailbox with the partial last line (i.e. missing a
# newline).  To do so, strip two last bytes from the orig.mbox and
# append the message to the resulting mailbox (nl2.mbox).  The partial
# message should become properly padded and terminated after the append.
#

AT_CHECK([bs=$(wc -c orig.mbox | sed -e 's/^ *//' -e 's/ .*//')
dd if=orig.mbox of=nl2.mbox bs=$((bs - 2)) count=1
],
[0],
[ignore],
[ignore])

AT_CHECK([mbop -m nl2.mbox < commands],
[0],
[append: OK
count: 4
4 current message
4 uid: 9
4 env_date: Mon Jul 29 21:00:09 2002
4 env_sender: alice@wonder.land
4 attr: -
4 headers: Received:(from alice@wonder.land) by wonder.land id 3304 for hare@wonder.land; Mon, 29 Jul 2002 22:00:09 +0100
Date:Mon, 29 Jul 2002 22:00:04 +0100
From:Alice  <alice@wonder.land>
Message-Id:<200207292200.3304@wonder.land>
To:March Hare  <hare@wonder.land>
Subject:Re: Invitation
X-UID:9

4 body_text: Then it wasn't very civil of you to offer it

3 current message
3 body_lines: 1
3 body_size: 16
3 body_text: There isn't any

])

AT_CHECK([mbop -m nl2.mbox 3\; body_lines\; body_size\; body_text],
[0],
[3 current message
3 body_lines: 1
3 body_size: 16
3 body_text: There isn't any

])

AT_CLEANUP

